'use strict';

/**
 * @ngdoc directive
 * @name frontEndChallengeApp.directive:batchDevice
 * @description Modifies a property of many drivers selected from a table
 * # batchDevice
 */
angular.module('frontEndChallengeApp')
  .directive('batchDevice', function () {
    return {
      template: '<button class="btn btn-primary">BATCH UPDATE</button>',
      restrict: 'AE',
      scope: {
        device: '=',
        drivers: '='
      },
      link: function postLink($scope, element) {
        element.on('click', function() {
          $scope.drivers.forEach( function(driver) {
            if(driver.selected) {
              driver.device = $scope.device;
            }
          });

          $scope.$apply();
        });
      }
    };
  });
