'use strict';

/**
 * @ngdoc directive
 * @name frontEndChallengeApp.directive:batchOperation
 * @description Modifies a property of many drivers selected from a table
 * # batchOperation
 */
angular.module('frontEndChallengeApp')
  .directive('batchOperation', function () {
    return {
      template: '<button id="batch-operation-btn" class="btn btn-primary">BATCH UPDATE</button>',
      restrict: 'AE',
      scope: {
        status: '=',
        drivers: '='
      },
      link: function postLink($scope, element) {
        element.on('click', function() {

          $scope.drivers.forEach( function(driver) {
            if(driver.selected) {
              driver.status = $scope.status;
            }
          });

          $scope.$apply();
        });
      }
    };
  });
