'use strict';

/**
 * @ngdoc directive
 * @name frontEndChallengeApp.directive:batchDelete
 * @description Deletes a batch of drivers from the table
 * # batchDelete
 */
angular.module('frontEndChallengeApp')
  .directive('batchDelete', function ($timeout) {
    return {
      template: '<button id="deleteBatchButton" class="btn btn-danger pull-right">BATCH DELETE</button>',
      restrict: 'AE',
      scope: {
        drivers: '=',
        table: '='
      },
      controller: function($scope) {
        
      },
      link: function postLink($scope, element) {
        element.on('click', function() {

          /*if(!confirm("Are you sure you want to delete the selected drivers?")) {
            return;
          }*/
          $scope.drivers.forEach( function(driver) {
            if(driver.selected) {
              var index = $scope.drivers.indexOf(driver);
              //console.log($scope.drivers);
              if(index > -1) {

                $scope.drivers.splice(index, 1);
                $scope.table.reload();
                $scope.$apply();

              }
            }
          });
        });
      }
    };
  });
