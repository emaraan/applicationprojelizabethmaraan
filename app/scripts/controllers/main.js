'use strict';

/**
 * @ngdoc function
 * @name frontEndChallengeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontEndChallengeApp
 */
  angular.module('frontEndChallengeApp').controller('MainCtrl', MainCtrl);
  MainCtrl.$inject = ['$scope', '$filter', 'NgTableParams', 'Drivers'];
  function MainCtrl($scope, $filter, NgTableParams, Drivers) {
           
    $scope.tableParams = new NgTableParams({
      page: 1,
      count: 10
    }, {
      getData: function($defer, params) {
        Drivers.query(function(data){
          
          // Check if $scope.data is falsy and initialize if so (helps persist on client until refresh)
          if(!$scope.data) {
            $scope.data = data;
          }

          params.total(data.length);

          var orderedData = params.sorting() ?
            $filter('orderBy')($scope.data, params.orderBy()) :
            $scope.data;
          orderedData = params.filter() ?
            $filter('filter')(orderedData, params.filter()) :
            orderedData;
          $scope.orderedData = orderedData;

          params.total(orderedData.length);
          $defer.resolve($scope.drivers = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        });
      }
    });

    $scope.checkboxes = { 'checked': false, items: {} };

    $scope.$watch('checkboxes.checked', function(value) {
      angular.forEach($scope.orderedData, function(item) {
        if (angular.isDefined(item.id)) {
          $scope.checkboxes.items[item.id] = value;
        }
      });
    });
    
    $scope.$watch('checkboxes.items', function(values) {
      if (!$scope.drivers) {
        return;
      }

      for( var key in values ) {
        $scope.orderedData[key-1].selected = values[key];
      }

      var checked = 0, unchecked = 0,
        total = $scope.drivers.length;
      angular.forEach($scope.drivers, function(item) {
        checked   +=  ($scope.checkboxes.items[item.id]) || 0;
        unchecked += (!$scope.checkboxes.items[item.id]) || 0;
      });
      if ((unchecked === 0) || (checked === 0)) {
        $scope.checkboxes.checked = (checked === total);
      }

      angular.element(document.getElementById('select_all')).prop('indeterminate', (checked !== 0 && unchecked !== 0));
    }, true);

    $scope.driver = { data: $scope.data };

    $scope.devices = [
      { id: 1, name: 'Driver Device'},
      { id: 2, name: 'Company Issued Device'}
    ];

    $scope.dropdownDeviceSelected = function (device) {
      $scope.selectedDevice = device;
    };

    $scope.addDriver = function(newDriver) {
      newDriver.device = $scope.deviceDropdownAdd.name;
      $scope.data.unshift(newDriver);
      $scope.tableParams.reload();
    };
  }
