'use strict';

/**
 * @ngdoc service
 * @name frontEndChallengeApp.Drivers
 * @description Retrieves the drivers dataset
 * # Drivers
 * Factory in the frontEndChallengeApp.
 */
angular.module('frontEndChallengeApp')
  .factory('Drivers', function($resource) {
      return $resource('/data/data.json');
  });
