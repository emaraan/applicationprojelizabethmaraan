'use strict';

describe('Directive: batchOperation', function () {

  // load the directive's module
  beforeEach(module('frontEndChallengeApp'));

  var element,
    scope,
    directiveScope;

  var expectedNewStatus = "New Status";
  var expectedOldStatus = "Old Status";
  var expectedDrivers = [ 
    {name: "Bob", status: expectedOldStatus, selected: false}, 
    {name: "Tim", status: expectedOldStatus, selected: true}, 
    {name: "Jess", status: expectedOldStatus, selected: true}, 
    {name: "Joe", status: expectedOldStatus, selected: false} 
  ];

  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();

    scope.status = expectedNewStatus;
    scope.drivers = expectedDrivers;
    scope.extra = "HELLO";

    element = angular.element('<batch-operation status="status" drivers="drivers"></batch-operation>');
    element = $compile(element)(scope);

    directiveScope = element.isolateScope();
  }));

  describe('When the directive is loaded with some driver and device data', function() {

    it('should make hidden element visible with the correct button text value', function () {
      expect(element.text()).toBe('BATCH UPDATE');
    });

    it('should pass only status and drivers data from parent controller', function () {
      expect(directiveScope.status).toBe(expectedNewStatus);
      expect(directiveScope.drivers).toBe(expectedDrivers);
      expect(directiveScope.extra).toBe(undefined);
    });
  });

  describe('When the batch operation directive button is clicked', function() {
    beforeEach(inject(function ($timeout) {
      element.triggerHandler('click');
    }));

    it('should change all drivers who are selected to the scope status', function () {
      expect(directiveScope.drivers[0].status).toBe(expectedOldStatus);
      expect(directiveScope.drivers[1].status).toBe(expectedNewStatus);
      expect(directiveScope.drivers[2].status).toBe(expectedNewStatus);
      expect(directiveScope.drivers[3].status).toBe(expectedOldStatus);
    });
  });
  
});
