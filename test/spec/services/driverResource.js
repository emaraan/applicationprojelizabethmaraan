'use strict';

describe('Service: Drivers', function () {

  // load the service's module
  beforeEach(module('frontEndChallengeApp'));

  // instantiate service
  var Drivers;
  beforeEach(inject(function (_Drivers_) {
    Drivers = _Drivers_;
  }));

  it('should do something', function () {
    expect(!!Drivers).toBe(true);
  });

});
