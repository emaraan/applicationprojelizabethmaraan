'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('frontEndChallengeApp'));

  var MainCtrl,
    scope,
    filter

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $filter) {
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope,
      $filter: $filter
    });
  }));

  describe('When the Main Controller is constructed', function() {

    beforeEach(function() {
    });

    it('should attach a list of awesomeThings to the scope', function () {
      //TODO
    });
  });
});
